FROM node:16-alpine

WORKDIR /app

COPY index.js index.js
COPY package.json package.json
COPY controller controller
COPY routes routes
COPY tests tests

RUN npm install
 
CMD [ "node", "./index.js"]