Step 1 
  
  - clone Repositori
    => git clone https://gitlab.com/ebyantoo/express-fazztrack-2

  - cd express-fazztrack-2


Step 2

 - Create Docker File
   
    FROM node:16-alpine  = image nodejs in docker

    WORKDIR /app    = masuk ke folder app

    COPY index.js index.js          = mengcopykan file index.js

    COPY package.json package.json  = mengcopykan file package.json

    COPY controller controller      = mengcopykan folder controller

    COPY routes routes              = mengcopykan folder routes
    
    COPY tests tests                = mengcopykan folder tests

    RUN npm install                 = install package yang ada di dalam package.js
    
    CMD [ "node", "./index.js"]     = menjalankan index.js


Step 3: Build the Docker image

  - Build Docker Image

    sudo docker build -t fazztrack-2-node-1:latest .


Step 4: Run the Docker container locally:

  - Run Docker 

    docker run -p 3000:3000 -d fazztrack-2-node-1:latest